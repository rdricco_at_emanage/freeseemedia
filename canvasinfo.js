var canvasInfo = {
       domain: "TODO",
       phone: "+1 716 330 1335",
       address: "Geurdeland 17L 6673DR Andelst, Netherlands",
       companyName: "Strong Current Enterprises Limited",
       companiInfo1: "Strong Current Enterprises Limited",
       companiInfo2: "19-21 Crawford Street, Dept 706,",
       companiInfo3: "London, W1H 1PJ",
       companiInfo4: "CoC Number - 7245331",
       companiInfo5: "VAT Number - NL823330692B01",
       physicaladdress1: "Geurdeland 17L 6673DR Andelst",
       physicaladdress2: "the Netherlands"
   };
                
$(document).ready(function(){
 // console.log("fillCanvasInfo", canvasInfo);
 var textBaseline = "bottom",
     ratio = window.devicePixelRatio || 1, // check if we are drawing on the hiDPI screen
     iterateOverCanvas = function($el, key) {
      $el.each(function(index) {
             $el = $(this);
             $pseudoEl = $("<span />").text(canvasInfo[key]);
             $pseudoEl.insertAfter($el);

             fontSize = $pseudoEl.css("font-size");
             fontSizeInt = parseInt(fontSize, 10);
             // console.log("key", key, canvasInfo[key], $el, $pseudoEl, $($pseudoEl).outerHeight(), $pseudoEl.width);
             ctx = $el[0].getContext("2d");
             ctx.canvas.width = (parseInt($pseudoEl.width(), 10) + 5) * ratio;
             ctx.canvas.height = fontSizeInt * ratio;
             ctx.font = ctx.canvas.height + "px " + $pseudoEl.css("font-family");
         ctx.fillStyle = $pseudoEl.css("color");
             ctx.textBaseline = textBaseline;
             ctx.fillText(canvasInfo[key], 0, ctx.canvas.height);
         $pseudoEl.remove();
             $el
                 .css({
                     "width": ctx.canvas.width / ratio,
                     "height": ctx.canvas.height / ratio,
                     "margin-bottom":"-4px"
                 })
                 .removeClass("hide");
     });
     },
     $el, $pseudoEl, fontSize, fontSizeInt, ctx;
       // console.log(canvasInfo);
      for (var key in canvasInfo) {
            var element = $(".canv-" + key);
            // console.log(element);
            if (element && element.length !== 0)
                  iterateOverCanvas(element, key);
      }    
});