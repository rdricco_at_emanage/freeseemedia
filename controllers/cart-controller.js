define(['store', 'crm-services', 'meta-data-storage', 'controllers/_controller-helper', 'lodash.min'], function(store, CRMServices, metaDataStorage, ControllerHelper, lodash) {   
    
    //to keep the debugger happy    
    var _store = store;
    var _CRMServices = CRMServices;
    var _metaDataStorage = metaDataStorage;
    var _ControllerHelper = ControllerHelper;
    var _ = lodash;
    
    var CartController = function(){

        this.$id = null;            //asigned from app services loader
        this.$el = null;            //asigned from app services loader
        this.hookSystem = null;
        
        this.cartDisplayItems = [];
        this.productsData = [];
        this.cartDOMContainer = null;
        
        
        //Initialize controller variables and startup logic
        //-------------------------------------------------
        this.init = function(_hookSystem){     
            
            //If this is the minicart, we are not showing it on certain pages so stop execution            
            if(this.$el.hasClass('mini-cart-controller')){
                if(window.location.href.indexOf('/cart') != -1){
                    $(".header-search-inner").addClass('no-border');
                    $(".total-cart").addClass("hidden");
                    $('.total-cart-in').remove();
                    return;
                }
                if(window.location.href.indexOf('/account/') != -1){
                    $(".header-search-inner").addClass('no-border');
                    $(".total-cart").addClass("hidden");
                    $('.total-cart-in').remove();
                    return;
                }
            }
            
            
            //hooks are public callbacks that any controller can access and register to
            this.hookSystem = _hookSystem;
            
            this.cartDOMContainer = this.$el.find("#cart-status");
            
            //Load cart items from crm services instead here
            var self = this;
            
            _CRMServices.getCampaignProducts().then(function(res){
                self.productsData = res;
                
                _CRMServices.getShoppingCart().then(function(data){
                    //if the cart is not empty
                    if(data && data.cart && data.cart.products){                        
                        //for each product, match with shipping cost
                        data.cart.products.forEach(function(product){
                            var match = _.find(self.productsData, {productId: product.productId});
                            if(match){
                                var shipping = _.find(match.shippings, {shippingMethodId: product.shippingMethodId});
                                product.shipping = shipping || match.shippings[0];                                
                            }
                        });
                        
                        self.cartDisplayItems = data.cart.products;
                        self.render();
                    } 
                    
                    //if this is checkout page and there are no products
                    if(window.location.href.indexOf('checkout.html') != -1){
                        if(!data || !data.cart || !data.cart.products || data.cart.products.length == 0){
                            
                            //If this is checkout page, but returning from paypal
                            if (window.location.href.indexOf("success=true") == -1){
                                //return to cart page
                                window.location.href = '/cart.html';
                            }
                        }
                    }
                    
                    //if this is cart page and there is no products
                    if(window.location.href.indexOf('cart.html') != -1){
                        if(!data || !data.cart || !data.cart.products || data.cart.products.length == 0){
                            //hide the price and checkout button area
                            $(".cart-content-table").hide();
                            //show a message that the cart is empty
                            $(".cart-empty-container").removeClass("hidden");
                        }
                    }
                });
            });
            
            
            //setup listener events for hooks
            this.initHooks();
            
            //Scan this controllers DOM for data-click attributes and create click listeners
            _ControllerHelper.scanActions(this, this.$el);
        };
        
        
        //Initialize public hooks (functions) that can be called from other controllers
        //-----------------------------------------------------------------------------
        this.initHooks = function(){
            var self = this;
            
            //any controller can update the products in the cart
            this.hookSystem.addHook('cart', 'addItemToCart', this.$id, function(item){   
                //check metadata for image description and name TODO
                var updateInsteadOfAdd = false;
                
                var match = _.find(self.cartDisplayItems, {productId: item.productId});
                if(match){  //update quantity
                    match.quantity += item.quantity;
                    if(match.quantity > 10){
                        match.quantity = 10;
                    }
                    updateInsteadOfAdd = true;
                }
                else{   //add to cart              
                    
                    //find the shipping data and add it to item before add
                    var match = _.find(self.productsData, {productId: item.productId});
                    if(match){
                        var shipping = _.find(match.shippings, {shippingMethodId: item.shippingMethodId});
                        item.shipping = shipping || match.shippings[0];
                    }
                    
                    self.cartDisplayItems.push(item);     
                }
                self.render();
                
                $(".wrapper").addClass('loading');
                
                
                //ADD TO CART IF NOT IN YET
                if(updateInsteadOfAdd == false){
                    //User has added an item to the cart, send the api request to save this product in the cart
                    _CRMServices.addItemToCart(item).then(function(){
                        $(".wrapper").removeClass('loading');
                        $(".total-cart-in").addClass('added-show');
                        setTimeout(function(){
                            $(".total-cart-in").removeClass('added-show');
                        }, 3000);
                    }).catch(function(res){
                        $(".wrapper").removeClass('loading');
                        $(".total-cart-in").addClass('added-show');
                        setTimeout(function(){
                            $(".total-cart-in").removeClass('added-show');
                        }, 3000);
                    });    
                }
                else{
                    //UPDATE QUANTITY IF ALREADY IN CART
                    _CRMServices.updateProductQuantity(item.productId, match.quantity).then(function(){
                        $(".wrapper").removeClass('loading');
                        $(".total-cart-in").addClass('added-show');
                        setTimeout(function(){
                            $(".total-cart-in").removeClass('added-show');
                        }, 3000);
                    }).catch(function(res){
                        $(".wrapper").removeClass('loading');
                        $(".total-cart-in").addClass('added-show');
                        setTimeout(function(){
                            $(".total-cart-in").removeClass('added-show');
                        }, 3000);
                    }); 
                }
            });
        };
        
        this.render = function(){
            var self = this;
            
            //output data model to html
            var container = $("#cart-status");
            var template = $(".cart-item-template");
            container.html('');
            
            $(".cart-quantity").html(this.cartDisplayItems.length);
            
            //Total $ value
            $(".cart-total-span").html(this.calcTotal());
            
            //Show shipping total separate if on checkout page
            if($(".cart-item-template.checkout-cart-page").length != 0){
                //calculate shipping cost total
                var shippingTotal = 0;
                this.cartDisplayItems.forEach(function(item){
                    shippingTotal += item.shipping.price * item.quantity;
                });  
                
                //Format Currency
                var formattedCost = shippingTotal.toFixed(2) + "";
                
                var currency = this.cartDisplayItems[0].currencyCode;
                if(currency == "USD"){
                    formattedCost = "$" + formattedCost;
                }                
                else if(currency == "EUR"){
                    formattedCost = (formattedCost + "â‚¬").replace(/\./g, ',');
                }
                else if(currency == "GBP"){
                    formattedCost = ("Â£" + formattedCost);
                }
                
                $(".shipping-cost-span").html(formattedCost);
            }
            
            //for each item in the cart, create dom element
            this.cartDisplayItems.forEach(function(product){
                var newProd = template.clone();
                
                var pDetails = self.mapPropertiesFromId(product.productId);
                
                newProd.attr('style', '').removeClass('cart-item-template')
                newProd.find(".cart-product-name").html(pDetails.name);
                newProd.find(".cart-item-quantity").html(product.quantity);
                newProd.find('.cart-product-name').attr('href', window.settingsBasePath + pDetails.href);
                newProd.find('.cart-img-link').attr('href', window.settingsBasePath + pDetails.href);
                newProd.find('.cart-img').attr('src', window.settingsBasePath + '/pub-assets/img/cart/' + pDetails.img);
                
                
                // Main cart page (cart.html)
                //---------------------------
                
                if(newProd.hasClass('main-cart-page')){
                    var currency = product.currencyCode;
                    
                    //Main price
                    newProd.find(".price-of-one").html(self.convertCurrencyPrice(product.price, currency));
                    newProd.find(".cart-plus-minus-box").attr('value', '' + product.quantity);
                    
                    //Shipping price
                    var shippingPrice = product.shippingPrice * product.quantity;
                    newProd.find(".shipping-item-price").html(self.convertCurrencyPrice(shippingPrice, currency));
                    
                    //Total Price
                    var totalPrice = ((product.price * product.quantity) + shippingPrice);
                    newProd.find('.total-item-price').html(self.convertCurrencyPrice(totalPrice, currency));
                    
                    //set up quantity changed listener
                    newProd.find(".cart-plus-minus-box").change(function(){
                       console.log($(this)); 
                        //api call, and set loading
                        $("#minicart, .shop-section").addClass('loading');
                        product.quantity = parseInt($(this).val());
                        
                        if(product.quantity > 10){
                            product.quantity = 10;
                            $(this).val(10);
                        }
                        
                        if(isNaN(product.quantity))
                            product.quantity = 1;
                        
                        if(product.quantity  < 1){
                            //delete instead
                            CRMServices.removeItemFromCart(product.productId).then(function(res){
                                $("#minicart, .shop-section").removeClass('loading');
                                self.cartDisplayItems.splice(self.cartDisplayItems.indexOf(product), 1);
                                self.render();
                            });
                        }
                        else{
                            CRMServices.updateProductQuantity(product.productId, product.quantity).then(function(res){
                                $("#minicart, .shop-section").removeClass('loading');
                                self.render();
                            });
                        }                        
                    });
                }
                
                
                // Checkout page mini cart (checkout.html)
                //-------------------------------------------
                
                if(newProd.hasClass('checkout-cart-page')){
                    var totalPrice = ((product.price * product.quantity));
                    newProd.find('.total-item-price').html(self.convertCurrencyPrice(totalPrice, product.currencyCode));
                }
                
                
                //set up delete click listener
                //----------------------------
                
                if(newProd.find(".zmdi-close").length != 0){
                    newProd.find(".zmdi-close").click(function(){
                        //set cart to loading and fire event for crm remove product
                        $("#minicart, .shop-section").addClass('loading');

                        CRMServices.removeItemFromCart(product.productId).then(function(res){
                            $("#minicart, .shop-section").removeClass('loading');
                            self.cartDisplayItems.splice(self.cartDisplayItems.indexOf(product), 1);
                            self.render();
                        });
                    });
                }
                
                
                newProd.appendTo(container);
            });
            
            if(this.cartDisplayItems.length == 0){
                $(".cart-content-table").hide();
                //show a message that the cart is empty
                $(".cart-empty-container").removeClass("hidden");
            }
        };
        
        //RaptorVR - 388 - SmartPhone VR Virtual Reality Glasses
        //TVFox - 312 - Generic TV Antenna
        //TVFrog - 313 - Home Theater Box V2 (Black)
        //EZSTREAMPAD - 444
        
        this.mapPropertiesFromId = function(id){
            switch(id){
                case 312: 
                    return {name: 'TVSurf Antenna', img: 'tv-fox.jpg', href: window.settingsBasePath + '/product-tvsurf.html'};
                case 313: 
                    return {name: 'TVRoxx', img: 'tv-frog.jpg', href: window.settingsBasePath + '/product-tvroxx.html'};
                case 388: 
                    return {name: 'Raptor VR', img: 'raptor-vr.jpg', href: window.settingsBasePath + '/product-raptorvr.html'};
                case 444: 
                    return {name: 'EZ StreamPad', img: 'ez-streampad.jpg', href: window.settingsBasePath + '/product-ezstreampad.html'};
                case 314: 
                    return {name: 'TVSurf Amplifier', img: 'tv-fox-amp.jpg', href: window.settingsBasePath + '/product-tvsurfamplifier.html'};
                case 393: 
                    return {name: 'Raptor VR Controller', img: 'raptorvr-controller.jpg', href: window.settingsBasePath + '/product-raptorvrcontroller.html'};
            }
        };
        
        //Calc total
        this.calcTotal = function(){
            var total = 0;
            this.cartDisplayItems.forEach(function(item){
                //var cost = 0;
                if(item.price){
                    total += item.price * item.quantity;   
                    total += item.shipping.price * item.quantity;
                } 
            });
            
            //Format Currency
            var formattedCost = total.toFixed(2) + "";

            var currency = this.cartDisplayItems[0].currencyCode;
            if(currency == "USD"){
                formattedCost = "$" + formattedCost;
            }                
            else if(currency == "EUR"){
                formattedCost = (formattedCost + "â‚¬").replace(/\./g, ',');
            }
            else if(currency == "GBP"){
                formattedCost = ("Â£" + formattedCost);
            }
            
            //Show disclaimer if its there
            if($('.usd-warning').length != 0){
                if(currency == "USD"){
                    $('.usd-warning').html("All pricing is in US Dollar");
                }                
            }
            
            return formattedCost;
        };
        
        this.convertCurrencyPrice = function(value, currency){
            var formattedCost = value.toFixed(2) + "";

            if(currency == "USD"){
                formattedCost = "$" + formattedCost;
            }                
            else if(currency == "EUR"){
                formattedCost = (formattedCost + "â‚¬").replace(/\./g, ',');
            }
            else if(currency == "GBP"){
                formattedCost = ("Â£" + formattedCost);
            }
            
            return formattedCost;
        };
        
        //Private controller functions called from dom
        //--------------------------------------------
        this.gotoCheckOut = function(){
            //console.log(this.cartDisplayItems);
            //debugger;
        };
        
        this.removeLastItem = function(){
            _CRMServices.removeItemFromCart('917671').then(function(res){
                console.log("Item removed successfully! ", res);
            });            
        };
        
    };
    
    return CartController;
});