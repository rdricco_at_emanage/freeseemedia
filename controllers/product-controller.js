//product controller defined module for require js
define(['crm-services', 'controllers/_controller-helper'], function(CRMServices, ControllerHelper) {   
    var _CRMServices = CRMServices;
    var _ControllerHelper = ControllerHelper;   //CRMServices - function holding all loaded data and functions for crm integratiom, element - a jquery element of the component div
    
    var ProductController = function(){

        //Controller Properties
        //---------------------   
        this.$id = null;            //asigned from app services loader
        this.$el = null;            //asigned from app services loader
        this.hookSystem = null;
        
        this.productId = null;
        this.priceDetails = null;
        this.basePrice = null;
        this.quantity = 1;
        
        this.currency = "";


        //Init (Map attributes and show correct data from crm services)
        //------------------------------------------------------------
        this.init = function(_hookSystem){     
            
            //hooks are public callbacks that any controller can access and register to
            this.hookSystem = _hookSystem;
            
            //map attributes
            this.productId = parseInt(this.$el.attr('data-product-id'));
            
            $(".plus-minus-pro-action").addClass('loading');
            
            var self = this;            
                        
            //GET (if no loaded) and show crm prices
            _CRMServices.getCampaignProducts().then(function(data){
                $(".add-button").removeClass('loading');
                $(".plus-minus-pro-action").removeClass('loading');
                
                for(var i = 0; i < data.length; i ++){
                    if(data[i].productId == self.productId){
                        var price = data[i].productPrices.DiscountedPrice.FormattedValue;
                        self.basePrice = data[i].productPrices.DiscountedPrice.Value;
                        self.$el.find('.price-container').html(price);
                        
                        if(price.indexOf("$") != -1){
                            self.currency = "$";
                        }
                        else if(price.indexOf("â‚¬") != -1){
                            self.currency = "â‚¬";
                        }
                        if(price.indexOf("Â£") != -1){
                            self.currency = "Â£";
                        }
                        
                        self.priceDetails = data[i];
                        self.priceDetails.defaultShippingMethod = self.priceDetails.shippings[0].shippingMethodId;
                        break;
                    }
                }
                //this product price = data.find product id
            });
            
            this.$el.find(".cart-plus-minus-box").change(function(){
                self.updateCostQuantity();
            });
            
            this.$el.find(".inc.qtybutton").click(function(){
                self.updateCostQuantity();
            });
            
            this.$el.find(".dec.qtybutton").click(function(){
                self.updateCostQuantity();
            });
            
            ////Scan this controllers DOM for data-click attributes and create click listeners
            _ControllerHelper.scanActions(this, this.$el);
        };
        
        this.updateCostQuantity = function(){
            if(this.$el.find(".cart-plus-minus-box").val()){
                if(parseInt(this.$el.find(".cart-plus-minus-box").val()) > 10){
                    this.$el.find(".cart-plus-minus-box").val(10);
                }                    
            }
            
            var el = $(this.$el);
            if(el.find(".cart-plus-minus-box").val()){
                var cost = this.basePrice * parseInt(el.find(".cart-plus-minus-box").val());
                 
                var formattedCost = cost.toFixed(2) + "";
                
                if(this.currency == "$"){
                    formattedCost = "$" + formattedCost;
                }                
                else if(this.currency == "â‚¬"){
                    formattedCost = (formattedCost + "â‚¬").replace(/\./g, ',');
                }
                else if(this.currency == "Â£"){
                    formattedCost = ("Â£" + formattedCost);
                }
                
                el.find('.price-container').html(formattedCost);
             }
        };
        
        this.render = function(){
            $(".cart-plus-minus-box").val(this.quantity);
        };
        
        //Initialize public hooks (functions) that can be called from other controllers
        //-----------------------------------------------------------------------------
        this.initHooks = function(){
            //No public hooks yet
        };
        
        
        //Private controller functions called from dom
        //--------------------------------------------
        this.addToCart = function(){
            
            var q = 1;
            
            if(this.$el.find(".cart-plus-minus-box").val()){
                q = parseInt(this.$el.find(".cart-plus-minus-box").val());
            }
            
            if(q == 0)
                return;
            
            if(q > 10){
                q = 10;
                this.$el.find(".cart-plus-minus-box").val(10);
            }
            
            this.$el.find(".add-button").addClass('loading');
            var self = this;
            setTimeout(function(){
                self.$el.find(".add-button").removeClass('loading');
            }, 2000);
            
            //Add this item to cart, and increment cart count component
            this.hookSystem.fire('cart', 'addItemToCart', 
                 {
                    productId: this.productId, 
                    quantity: q, 
                    shippingMethodId: this.priceDetails.defaultShippingMethod, 
                    productName: this.priceDetails.productName,
                    price: this.priceDetails.productPrices.DiscountedPrice.Value
                 }
            );
        };
        
        
//        this.increaseQuantity = function(){
//            this.quantity++;
//            this.render();
//        };
//        
//        this.decreaseQuantity = function(){
//            this.quantity--;
//            if(this.quantity < 0)
//                this.quantity = 1;
//            this.render();
//        };
        
    };
    
    return ProductController;
});