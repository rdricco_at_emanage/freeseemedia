//Static helper class with functions to manipulate DOM, scan DOM for actions, etc.
//--------------------------------------------------------------------------------

define([], function(){
    var ControllerHelper = function(){
        
        //Scroll to element
        //-----------------
        this.scrollToEl = function($el, speed){
            speed = speed || 1000;
                if ($el.length) {
                    $("html, body")
                        .stop()
                        .animate({
                            scrollTop: parseInt($el.offset().top, 10),
                        }, speed);
                }
        };
        
        //Set prices on element
        //---------------------
        this.setPrices = function($el, data, target){   //component element, price data, and target container (optional)
            //TODO
            //if(!target) target = '#price-container';
            $el.find(target).html(data);
        };
            
        
        //Scan component DOM for data-click callbacks and set click event listeners
        //---------------------------------------------------------------------
        this.scanActions = function(scope, $el){
            $el.find('[data-click]').each(function(){            
                var button = $(this);
                
                var func = button.attr('data-click');
                if(!func){
                    console.warn("Misconfigured click handler! ", button);
                    return;
                }
                func = func.substr(0, func.indexOf('('));
                
                var args = [];      //TODO parse from string and create array object
                
                if(scope[func]){
                    button.click(function(){
                        scope[func]();       //call with closure for 'this'   
                    });
                }
            });
        };    
    };
    
    //singleton service
    return new ControllerHelper();
});