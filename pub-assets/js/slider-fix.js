(function(){
    setTimeout(function(){
    
        //landing page
        function changeSlider() {
            if($(".banner-section").width() <= 480) {
                $(".banner-section").find(".slick-initialized").removeClass("active-related-product");
            } else {
                $(".banner-section").find(".slick-initialized").addClass("active-related-product");
            }
        
            //Single product details. related products
            if($(".related-product-area").width() <= 480) {
                $(".related-product-area").find(".slick-initialized").removeClass("active-related-product");
            } else {
                $(".related-product-area").find(".slick-initialized").addClass("active-related-product");
            }
        }
        
        changeSlider();
        
        $(window).resize(changeSlider);
        
        
    },1000);
})();