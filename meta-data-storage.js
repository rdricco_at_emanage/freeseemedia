define(['store'], function(store, test){
    var metaDataStorage = function(){
        
        this.storage = {};
        
        this.set = function(component, obj){
            
            if(!this.storage[component]){
                this.storage[component] = {};
            }
            
            this.storage[component][obj.name] = obj.value;
            
            //var prefix = window.settings 
            store.set('metaStorage', this.storage);     //save to local storage
        };
        
        this.get = function(component, key){            
            if(this.storage[component])
                return this.storage[component][key];
        };
        
        this.init = function(){            
            //var prefix = window.settings 
            this.storage = store.get('metaStorage') || {};
        };
        
    };
    
    //singleton service
    return new metaDataStorage();
});