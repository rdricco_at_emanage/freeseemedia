// documentation: https://websalesapi.tryemanage.com/swagger/ui/index#/

define(function(){    
    return function(){
        "use strict";

        var site = window.SITE,
            
        baseApiUrl = "https://emanage-prod-websales-api.azurewebsites.net/api/",    //Prod
        //baseApiUrl = "https://emanage-dev-websales-api.azurewebsites.net/api/", //Dev
        
        getPromise = function(options) {
            var finalOptions = $.extend(true, {
                method: "GET",
            }, options || {});
            return $.ajax(finalOptions);
        },
        endpoints = {
            location: function(wid) {
                return getPromise({
                    url: baseApiUrl + "campaigns/" + wid + "/customers/location",
                });
            },
            campaignSettings: function(wid) {
                return getPromise({
                    url: baseApiUrl + "campaigns/" + wid + "/settings",
                });
            },
            countries: function(wid) {
                return getPromise({
                    url: baseApiUrl + "campaigns/" + wid + "/countries",
                });
            },
            provinces: function(wid, country){
                return getPromise({
                    url: baseApiUrl + "campaigns/" + wid + "/countrystates?countryCode=" + country, 
                }); 
            },
            // pricesByCountryCode: function(wid, options) {
            //     var countryCode = options.countryCode || site.helpers.getParameterByName("countryCode");
            //     countryCode = countryCode !== "" ? "/"+countryCode : "";
            //     return getPromise({
            //         url: baseApiUrl + "campaigns/"+ wid +"/products" + countryCode,
            //     });
            // },
            putShipping: function(wid, options) {
                options.data = JSON.stringify(options.data);

                return getPromise({
                    url: baseApiUrl + "customers/" + wid + "?email=" + options.email,
                    method: "PUT",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: options.data,
                });
            },
            prices: function(wid, options) {
                var countryCode = options.countryCode,
                    locationData = site.helpers.storage.get("location");

                if (!countryCode && locationData) {
                    countryCode = locationData.countryCode;
                }
                countryCode = site.dataController.common.getLocalizedCountryCode(countryCode);
                countryCode = "" + countryCode !== "" ? "/" + countryCode : "";
                return getPromise({
                    url: baseApiUrl + "campaigns/" + wid + "/products/prices" + countryCode,
                });
            },
            prices2: function(wid, countryCode) {
                return getPromise({
                    url: baseApiUrl + "campaigns/" + wid + "/products/prices/" + countryCode,
                });
            },
            discountPrice: function(wid, options) {
                return getPromise({
                    url: baseApiUrl + "campaigns/" + wid + "/coupons/" + options.couponCode + "/calculate" + site.helpers.getQueryStringByObject(options.data),
                });
            },
            postCustomer: function(wid, options) {
                return getPromise({
                    url: baseApiUrl + "customers/" + wid,
                    method: "POST",
                    data: options.data,
                    // uses OPTIONS preflighted request
                    // data : JSON.stringify(options.data),
                    // contentType : "application/json",
                });
            },
            customer: function(wid, options) {
                // filter can be id, email or fullname
                return getPromise({ // params should be added to encodeURIComponent, but this should be added in other branch.
                    url: baseApiUrl + "customers/" + site.helpers.getQueryStringByObject(options.filter),
                });
            },
            postOrder: function(wid, options) {
                var isTest = "";
                if (site.dataController.common.isCardTest()) {
                    if (site.helpers.storage.get("paymentProcessorId") === 5 || site.helpers.storage.get("paymentProcessorId") === 4) {
                        isTest = "?behaviorId=2";
                    }
                }
                options.data = JSON.stringify(options.data);
                return getPromise({
                    url: baseApiUrl + "orders/" + wid + isTest,
                    method: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: options.data,
                });
            },
            putOrder: function(wid, options) {
                var isTest = "";
                if (site.dataController.common.isCardTest()) {
                    isTest = "&isTest=true";
                }
                return getPromise({
                    url: baseApiUrl + "orders/" + wid + "?trackingNumber=" + options.trackingNumber + isTest,
                    method: "PUT",
                    data: options.data,
                });
            },
            getOnpex: function(wid, options) {
                var url = baseApiUrl + "callback/onpex?mcTxId=" + options.orderId;
                return getPromise({
                    url: url,
                });
            },
            getOrderStatus: function(wid, options) {
                var url = baseApiUrl + "payments/transactions/" + options.orderId;
                if (site.dataController.common.isThreeDSFake()) {
                    url = "https://fe.dfograpevinetest.com/transactionsCheck.php?tr=" + options.orderId;
                }
                return getPromise({
                    url: url,
                });
            },
            miniUpsells: function(wid, options) {
                return getPromise({
                    url: baseApiUrl + "campaigns/" + wid + "/products/" + options.productId + "/miniupsells",
                });
            },
            miniUpsells2: function(wid, options) {
                return getPromise({
                    url: baseApiUrl + "campaigns/" + wid + "/products/" + options.productId + "/miniupsells",
                });
            },
            upsells: function(wid, options) {
                return getPromise({
                    url: baseApiUrl + "campaigns/" + wid + "/upsells",
                });
            },
            localities: function(wid, options) {
                if (!options.data || !options.data.countryCode) {
                    console.warn("No countryCode supplied for `localities` endpoint");
                    return getPromise();
                }
                if (!options.data.lang) {
                    options.data.lang = site.settings.lang;
                }

                return getPromise({
                    url: baseApiUrl + "campaigns/" + wid + "/countrystates",
                    data: options.data,
                });
            },
            localization: function(wid, options) {
                options.data = options.data || {};
                if (!options.data.lang) {
                    options.data.lang = site.settings.lang;
                }

                return getPromise({
                    url: "https://api.cussercarforu.com/api/GetFormSerial" + site.helpers.getQueryStringByObject(options.data),
                    dataType: "jsonp",
                });
            },
            postAffiliate: function(wid, options) {
                return getPromise({
                    url: baseApiUrl + "campaigns/" + wid + "/affiliate",
                    method: "POST",
                    data: options.data,
                });
            },
            
            confirmPaypalPayment: function(wid, id){    
                return getPromise({
                    url: baseApiUrl + "shoppingcart/" + wid + "/confirmpayment?trackingNumber=" + id + "&paymentProvider=5",
                    method: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                });
            },

            //Shopping Cart
            //-------------
            //This creates a shopping cart and adds a product, cannot create an empty shopping cart
            createShoppingCart: function(wid, product){                
                var data = {
                    behaviorId: 2,      //1 prod, 2 test
                    product: {
                      productId: product.productId,
                      quantity: product.quantity,
                      shippingMethodId: product.shippingMethodId
                    }
                };
                data = JSON.stringify(data);
                return getPromise({
                    url: baseApiUrl + "shoppingcart/" + wid + "/new",
                    method: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: data,
                });
            },
            addItemToCart: function(wid, sessionId, product){
                var data = JSON.stringify({
                          productId: product.productId,
                          quantity: product.quantity,
                          shippingMethodId: product.shippingMethodId
                        });
                
                return getPromise({
                    url: baseApiUrl + "shoppingcart/" + wid + "/add?sessionId=" + sessionId,
                    method: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: data,
                });
            },
            removeItemToCart: function(wid, sessionId, productId){
                return getPromise({
                    url: baseApiUrl + "shoppingcart/" + wid + "/remove?sessionId=" + sessionId + "&productId=" + productId,
                    method: "DELETE"
                });
            },
            updateProductQuanitityShoppingCart: function(wid, sessionId, productId, newQuantity){
                return getPromise({
                    url: baseApiUrl + "shoppingcart/" + wid + "/update?sessionId=" + sessionId + "&productId=" + productId + "&quantity=" + newQuantity,
                    method: "PUT"
                });
            },
            getShoppingCart: function(wid, sessionId){
                return getPromise({
                    url: baseApiUrl + "shoppingcart/" + wid + "?sessionId=" + sessionId,
                });
            },
            createAndLinkShoppingCartCustomer: function(wid, sessionId, customer){                
                var data = JSON.stringify(customer);
                return getPromise({
                    url: baseApiUrl + "shoppingcart/" + wid + "/customers/new?sessionId=" + sessionId,
                    method: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: data,
                });
            },
            placeOrderShoppingCart: function(wid, data){
                var data = JSON.stringify(data);
                return getPromise({
                    url: baseApiUrl + "shoppingcart/" + wid + "/checkout",
                    method: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: data,
                });
            }
            //-------------
            //Shopping Cart            
        },
        exports = {
            get: function(endpointName) {
                return endpoints[endpointName];
            },
        };

        site.cloudCrm = site.cloudCrm || {};
        site.cloudCrm.endpoints = exports;   
    }(); 
});