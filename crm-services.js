//make sure endpdoints.js are loaded by now
define(['endpoints', 'store', 'lodash.min'], function(endpoints, store, _) {
    var CRMServices = function(){

        this.campaignWebKey = "1ca76828-a200-4d25-83ec-00190b92a53e";
        //this.campaignWebKey = "f9e05ada-0a66-4a64-a4ea-a222d1c1f795";
        
        this.campaignData = {
            location: null,
            products: null            
        };
        
        this.shoppingCartData = {
            shoppingCartSession: null,
            cart: {},
            hasLinkedCustomer: null
        };

        this.creatingShoppingCart = false;
        this.promises = {};

        
        this.getCurrentCountry = function(){
            return this.campaignData.location;  
        };
        
        //This function returns products and prices from the campaign
        //First by loading users location, and second by loading prices
        //Each step returns a promise that is resolved with the final data
        this.getCampaignProducts = function() {
            var self = this;

            //Products has not been loaded yet
            if(!this.campaignData.products){

                //if there is no promise object yet for this data...
                if(!this.promises['products'])
                    this.promises['products'] = $.Deferred();
                            
                //First we need to load location, only load if not yet loaded and loading is not in progress
                if(!this.campaignData.location && !this.promises['location']){
                    //if no location, need to GET first and then load products after
                    var func = window.SITE.cloudCrm.endpoints.get('location');                    
                    this.promises['location'] = func(this.campaignWebKey).then(function(res){
                        self.campaignData.location = res;
                        return res;                    
                    }).then(function(data){
                        func = window.SITE.cloudCrm.endpoints.get('prices2');
                        func(self.campaignWebKey, data.countryCode).then(function(products){
                            self.campaignData.products = products;
                            //resolve promises for all controllers waiting for this data
                            self.promises['products'].resolve(products);
                            self.saveCRMDataToCache();          //cache and save all products and prices and location
                        });    
                    });   
                }

                return this.promises['products'];
            }

            //products is already loaded, return a promise with the data
            return $.when( this.campaignData.products );
        };
        
        this.getCountries = function(){
            var func = window.SITE.cloudCrm.endpoints.get('countries');
            return func(this.campaignWebKey); 
        };
        
        this.getProvinces = function(){
            var func = window.SITE.cloudCrm.endpoints.get('provinces');
            return func(this.campaignWebKey, this.campaignData.location.countryCode); 
        };

        this.getSelectedProvinces = function(countryCode){
            var func = window.SITE.cloudCrm.endpoints.get('provinces');
            return func(this.campaignWebKey, countryCode);
        }; 

        this.confirmPayPalPayment = function (id) {
            var func = window.SITE.cloudCrm.endpoints.get('confirmPaypalPayment');
            return func(this.campaignWebKey, id);
        };      
        
        //Shopping Cart Integration
        //-------------------------
        
        this.addItemToCart = function(product){
            var self = this;
            
            //first, is there a shopping cart created yet?
            if(this.shoppingCartData.shoppingCartSession){                
                var func = window.SITE.cloudCrm.endpoints.get('addItemToCart');                    
                return func(this.campaignWebKey, this.shoppingCartData.shoppingCartSession, product).then(function(res){
                    console.log("added to cart succesfully!", res);
                    self.refreshCartStatus();
                    return res;                    
                });
            }
            else{       //otherwise, no shopping cart yet, check if it is loading or there is not even one
                if(this.promises['shoppingCartSession']){       //is a GET in progress to load this cart session?  
                    console.log("Creating and added...");
                    this.promises['products' + product.productId] = $.Deferred();       //a promise to return to the function who called this one... but we will get back to it after shopping cart loads
                    this.promises['shoppingCartSession'].then(function(res){                        
                        //shoping cart has loaded, and now we should have an ID, start the add request again
                        var func = window.SITE.cloudCrm.endpoints.get('addItemToCart');                    
                        func(self.campaignWebKey, self.shoppingCartData.shoppingCartSession, 
                                    product).then(function(data){
                            console.log("added to cart succesfully, after shopping cart loaded!", data);
                            self.refreshCartStatus();
                            self.promises['products' + product.productId].resolve(data); 
                            self.promises['products' + product.productId] = null;
                        });                        
                    });                    
                    return this.promises['products' + product.productId];
                }
                else{   //otherwise, create a new cart session which will add this item automatically
                    return this.addToAndCreateShoppingCart(product);
                }
            }
        };
        
        //this is only called from addItemToCart if there is 0 items in cart and cart is not created yet
        this.addToAndCreateShoppingCart = function(product){
            var self = this;
            var func = window.SITE.cloudCrm.endpoints.get('createShoppingCart');                    
            this.promises['shoppingCartSession'] = func(this.campaignWebKey, product).then(function(res){
                self.shoppingCartData.shoppingCartSession = res;
                console.log("cart succesfully!", res);  //save this cart ID to local storage
                store.set('shoppingCartSession', res); 
                
                //start GET to keep shopping cart object as up to date as possible
                self.refreshCartStatus();
                
                return res;
            });
            
            return this.promises['shoppingCartSession'];
        };
        
        //GET the up to date data from the campaign get....
        this.refreshCartStatus = function(){
            if(this.shoppingCartData.shoppingCartSession){
                var self = this;
                var func = window.SITE.cloudCrm.endpoints.get('getShoppingCart');                    
                return func(this.campaignWebKey, this.shoppingCartData.shoppingCartSession).then(function(res){
                    self.shoppingCartData = {
                        shoppingCartSession: res.sessionId,
                        cart: res
                    };
                });
            }
        };
        
        //Remove item from cart
        this.removeItemFromCart = function(productId){
            var func = window.SITE.cloudCrm.endpoints.get('removeItemToCart');                    
            return func(this.campaignWebKey, this.shoppingCartData.shoppingCartSession, productId);
        };
        
        //change quantity for item in cart
        this.updateProductQuantity = function(productId, newQuantity){
            var func = window.SITE.cloudCrm.endpoints.get('updateProductQuanitityShoppingCart');  
            return func(this.campaignWebKey, this.shoppingCartData.shoppingCartSession, productId, newQuantity);
        };

        this.placeOrderPaypal = function(){
            //cannot place order with no products
            if(this.shoppingCartData.cart.products.length == 0){
                console.error("Can not place order with an empty cart");
                return;
            }
            
            var self = this;

            var data = {
                sessionId: this.shoppingCartData.shoppingCartSession,
                payment: {
                    paymentProcessorId: 5, 
                }
            };

            func = window.SITE.cloudCrm.endpoints.get('placeOrderShoppingCart');  
            return func(self.campaignWebKey, data).then(function(data){
                return data;
            });      
        };

        //Check out with customer information
        this.placeOrderShoppingCart = function(data){            
            
            //cannot place order with no products
            if(this.shoppingCartData.cart.products.length == 0){
                console.error("Can not place order with an empty cart");
                return;
            }
            
            var self = this;
            
            //create a promise to return since the real request is after create customer request
            if(!this.promises['placeOrderShoppingCart'])
                    this.promises['placeOrderShoppingCart'] = $.Deferred();
            
            
            //if(!this.shoppingCartData.hasLinkedCustomer){
                //First we either create a new customer or link this shopping cart ID to an existing customer
                var func = window.SITE.cloudCrm.endpoints.get('createAndLinkShoppingCartCustomer');  
                func(this.campaignWebKey, this.shoppingCartData.shoppingCartSession, data.customer).then(function(res){
                    //Customer is either created or updated automatically on backend, and now this session id is linked to the customer

                    self.shoppingCartData.hasLinkedCustomer = true;
                    store.set('shoppingCartSession', self.shoppingCartData);

                    self.placeShoppingCartOrderAfterLink(data);           
                }).catch(function(err){
                    console.log(err);
                    self.promises['placeOrderShoppingCart'].resolve(err); 
                    self.promises['placeOrderShoppingCart'] = null;
                });
            //}
            //else{
            //    self.placeShoppingCartOrderAfterLink(data);     //skip customer step since it is already linked and no longer available
            //}
            
            return this.promises['placeOrderShoppingCart'];
        };
        
        this.placeShoppingCartOrderAfterLink = function(data){
            var self = this;
            
            var orderDetails = {
                sessionId: self.shoppingCartData.shoppingCartSession,
                payment: data.payment
            };

            //data.payment

            //now place the order
            func = window.SITE.cloudCrm.endpoints.get('placeOrderShoppingCart');  
            func(self.campaignWebKey, orderDetails).then(function(data){
                self.promises['placeOrderShoppingCart'].resolve(data); 
                self.promises['placeOrderShoppingCart'] = null;
                //once order is placed, clear shopping cart session
                if (data && data.success === true) {
                    self.clearShoppingCart();
                }                    
            }).catch(function(err){
                console.log(err);
                self.promises['placeOrderShoppingCart'].resolve(err); 
                self.promises['placeOrderShoppingCart'] = null;
            });
        };
        
        //Get shopping cart, for display purposes etc.
        this.getShoppingCart = function(){
            var self = this;
            
            //if loading GET in progress... return that promise
            if(this.promises['shoppingCartSession'] && this.promises['shoppingCartSession'].state() != 'resolved'){
                return this.promises['shoppingCartSession'].then(function(res){
                    return res;
                }).catch(function(res){
                    console.error(res.responseText);
                    self.clearShoppingCart();
                });
            }
            
            //otherwise return the shopping cart data in promise form
            return $.when( this.shoppingCartData );
        };
        
        //Either cache is invalid or order is complete, delete shopping cart session id
        this.clearShoppingCart = function(){
            this.shoppingCartData = {
                shoppingCartSession: null,
                cart: {},
                hasLinkedCustomer: null
            };
            store.remove('shoppingCartSession');
        };
        
        //-------------------------
        //Shopping Cart Integration
        
        
        
        
        //Check cache and init
        this.init = function(){            
            
            //first check cache for CRMData
            var cache = store.get('CRMData');            
            if(cache){
                //check if its expired
                var expire = new Date(cache.timestamp).getTime();
                var now = new Date().getTime();
                
                if(expire < now){
                    if(cache.shoppingCartSession){  //preserve the shopping cart session id
                        this.campaignData = {shoppingCartSession: cache.shoppingCartSession};
                    }
                    store.remove('CRMData');  
                }
                else{           //otherwise, set as main data
                    this.campaignData = cache;
                }
            }                        
            
            //next check cache for cart session
            var session = store.get('shoppingCartSession');
            if(session){
                //shopping cart exists, GET the cart items 
                var self = this;
                var func = window.SITE.cloudCrm.endpoints.get('getShoppingCart');                 
                
                this.promises['shoppingCartSession'] = func(this.campaignWebKey, session).then(function(res){
                    self.shoppingCartData.cart = res;
                    self.shoppingCartData = {
                        shoppingCartSession: res.sessionId,
                        cart: res,
                        hasLinkedCustomer: session.hasLinkedCustomer                        
                    };
                    return self.shoppingCartData;                    
                });
            }
        };
        
        
        this.saveCRMDataToCache = function(){            
            //if there is no cache yet, create and add timestamp
            var cache = store.get('CRMData');
            if(!cache){
                var expire = new Date()
                expire.setDate(expire.getDate() + 1);    // add a day
                
                cache = {timestamp: expire};
                store.set('CRMData', cache);
            }
            
            //dont refresh the timestamp
            var dataToSave = $.extend({timestamp: cache.timestamp}, this.campaignData);
            dataToSave.promises = null;
            store.set('CRMData', dataToSave);            
        };


        this.getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
    };

    //singleton instance
    var services = new CRMServices();
    services.init();
    return services;
});