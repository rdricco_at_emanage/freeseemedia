(function(){
    
    var metaDataStorage;
    
    var componentsList = {};    
    var hookSystem;
    
    var init = function(){
        
        //If base path not set, use root
        if(!window.settingsBasePath)
            window.settingsBasePath = "";
        
        window.SITE = {};
        
        initHookSystem();
        
        requirejs(['crm-services', "meta-data-storage"], function(CRMServices, _metaDataStorage) {
            metaDataStorage = _metaDataStorage;
            metaDataStorage.init();                 //load local storage
            
            CRMServices.getCampaignProducts();      //GET and cache products and prices
            
            scanComponents();
        });
    };
    
    
    //After init, scan DOM for components and create and link controllers with attributes from the dom with document query selector   
    var scanComponents = function(){
        //for each component on this page
        $('[data-component-controller]').each(function(){            
            var componentDOM = $(this);
            
            loadAndLinkController(componentDOM);
            parseAndSaveMetaData(componentDOM);
        });
    };
    
    
    //function to load, link, and init a component's controller
    //--------------------------------------------------------
    var loadAndLinkController = function(componentDOM){
        //GET the controllers name and request the loaded js controller 
        var controllerName = componentDOM.attr('data-component-controller');

        //Component list will be a list of all components, with components of the same type in an array
        if(!componentsList[controllerName]){
            componentsList[controllerName] = [];
        }

        //Keep track of this component and increment ID
        var component = {
            id: controllerName + '-' + componentsList[controllerName].length, 
            controller: null            //connected later when controller is loaded, line 52
        };

        componentsList[controllerName].push(component);

        //Load the controller js for this component, and then initialize and link
        requirejs(["./controllers/" + controllerName], function(_controller) {
            var newController = new _controller();
            newController.$id = component.id;
            newController.$el = componentDOM;
            newController.init(hookSystem);     //initialize this controller with a reference to the hook system

            component.controller = newController;
        }); 
    };
    
    
    //function to scan and save meta data on a component
    //--------------------------------------------------
    var parseAndSaveMetaData = function(componentDOM){
        //Check if this component has meta data to save
        var storageKey = componentDOM.attr('data-meta-id');

        if(storageKey){     //if there is meta data id on this component then scan for all meta data
            //element scan for any data-meta-** attributes
            for(var i = 0; i < componentDOM[0].attributes.length; i ++){
                var attr = componentDOM[0].attributes[i];

                if(attr.name.indexOf('data-meta-') != -1){  //filter attributes that are not meta data
                    var variableName = attr.name.substr(10);       //get text after 'data-meta-'
                    variableName = variableName.replace(/-([a-z])/g, function (g) { return g[1].toUpperCase(); }); //convert to camelcase

                    metaDataStorage.set(storageKey, {name: variableName, value: attr.value });
                }
            }
        }
    };
    
    
    
    //Hooks and even system, components can register and call functions from each other through here
    //----------------------------------------------------------------------------------------------
    var initHookSystem = function(){
        
        //Create hook system class
        hookSystem = new function(){
            
            //List and groupings of all hooks and callbacks
            this.hooks = {};

            //Add a hook to the hook list, grouped by component and unique by component id
            this.addHook = function(componentName, hookName, componentID, callback){
                if(!this.hooks[componentName]){
                    this.hooks[componentName] = {};
                }
                if(!this.hooks[componentName][hookName]){
                    this.hooks[componentName][hookName] = [];
                }
                
                this.hooks[componentName][hookName].push({
                    id: componentID,
                    callback: callback
                });
            };

            //Find a hook and call the event with provided arguments
            //componentName - hookGroup to look in, hookName - name of function to call, args - {} array of arguments passed from component calling this hook
            this.fire = function(componentName, hookName, args){
                if(this.hooks[componentName] && this.hooks[componentName][hookName]){
                    //if more specific information is proved regarding which hook to call if duplicates, check, otherwise, fire the first hook we find matching the description
                    this.hooks[componentName][hookName][0].callback(args);
                }
            };
        };
        
    };
        
    $(document).ready(init);
})();